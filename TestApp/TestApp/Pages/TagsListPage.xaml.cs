﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;

namespace TestApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TagsListPage : ContentPage
	{
        private TagsListViewModel ViewModel;
		public TagsListPage (WordsListViewModel wordsListViewModel)
		{
            ViewModel = new TagsListViewModel(wordsListViewModel);
            BindingContext = ViewModel;
			InitializeComponent ();
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            tagsList.BeginRefresh();

            if (string.IsNullOrWhiteSpace(ViewModel.SearchedText))
            {
                ViewModel.TagsToDisplay = ViewModel.AllTags;
            }
            else
            {

                var SearchResults = new ObservableCollection<string>();
                SearchResults = new ObservableCollection<string>(ViewModel.AllTags.Where(i => i.ToLower().Contains(ViewModel.SearchedText.ToLower())));
                
                ViewModel.TagsToDisplay = SearchResults;
            }

            tagsList.EndRefresh();
        }
        
    }
}