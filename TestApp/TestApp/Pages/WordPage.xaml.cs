﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WordPage : ContentPage
	{
        public WordViewModel Word { get; set; }
		public WordPage (WordsListViewModel wordsListViewModel)
		{
            Word = new WordViewModel();
            Word.WordsListViewModel = wordsListViewModel;
            BindingContext = Word;
			InitializeComponent ();
            MessagingCenter.Subscribe<WordsListViewModel>(this, "InputValidation", (WordsListViewModel)=> {DisplayAlert("Error input", "Please input all required data, don't forget to add a minimum one tag","Ok"); });


        }

        public void AddTagButtonClicked(object sender,EventArgs args)
        {
            Tags.BeginRefresh();
            if (!string.IsNullOrWhiteSpace(AdditionalTagsEntry.Text))
            {
                Word.AddTag();
                AdditionalTagsEntry.Text = "";
            }
            Tags.EndRefresh();
        }
    }
}