﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;

namespace TestApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WordsListPage : ContentPage
	{   
        public WordsListViewModel ViewModel { get; set; }
		public WordsListPage ()
		{
            ViewModel = new WordsListViewModel() { Navigation = this.Navigation };
            BindingContext = ViewModel;
            InitializeComponent();
            
            wordsList.ItemAppearing += (sender, e) =>
            {
                
                if (ViewModel.WordsToDisplay.Count == 0 || !string.IsNullOrEmpty(ViewModel.SearchedText))
                    return;

                if (e.Item == ViewModel.WordsToDisplay[ViewModel.WordsToDisplay.Count - 1])
                {
                    ViewModel.LoadWords();
                }
            };
                Tags.GestureRecognizers.Add(new TapGestureRecognizer((view) => OnLabelClicked()));
        }

        private void OnLabelClicked()
        {
            ViewModel.ViewTags();
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            wordsList.BeginRefresh();

            if (string.IsNullOrWhiteSpace(ViewModel.SearchedText))
            {
                ViewModel.WordsToDisplay = new ObservableCollection<WordViewModel>();
                ViewModel.LoadWords();
            }
            else
            {

                var SearchResults = new ObservableCollection<WordViewModel>();
                if (ViewModel.SearchedText.TrimStart().StartsWith("#"))
                {
                    foreach(WordViewModel word in ViewModel.AllWords)
                    {
                        foreach(string tag in word.Tags)
                        {
                            if (tag.ToLower().Contains(ViewModel.SearchedText.ToLower()))
                            {
                                SearchResults.Add(word);
                            }
                        }
                    }
                }
                else
                {
                    SearchResults =new ObservableCollection<WordViewModel>(ViewModel.AllWords.Where(i => i.WordInEnglish.ToLower().Contains(ViewModel.SearchedText.ToLower())));
                }
                ViewModel.WordsToDisplay = SearchResults;
            }

            wordsList.EndRefresh();
        }
        
    }
}