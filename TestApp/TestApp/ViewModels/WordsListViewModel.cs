﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using System.Text;
using Xamarin.Forms;
using TestApp.Pages;
using System.Linq;

namespace TestApp.ViewModels
{
    public class WordsListViewModel:INotifyPropertyChanged
    {
        #region WordsListViewModel attributes
        public INavigation Navigation { get; set; }
        public ObservableCollection<WordViewModel> AllWords { get; set; }
        private ObservableCollection<WordViewModel> _WordsToDisplay;
        public event PropertyChangedEventHandler PropertyChanged;
        private WordViewModel _SelectedWord;
        private string _SearchedText;
        #endregion

        #region WordsListViewModel commands
        public ICommand AddWordCommand { get; set; }
        public ICommand SaveWordCommand { get; set; }
        public ICommand BackCommand { get; set; }
        public ICommand SearchWordCommand { get; set; }
        #endregion

        #region WordsListViewModel properties
        public ObservableCollection<WordViewModel> WordsToDisplay
        {
            get { return _WordsToDisplay; }
            set
            {
                if (_WordsToDisplay != value)
                {
                    _WordsToDisplay = value;
                    OnPropertyChanged("WordsToDisplay");
                }
            }
        }
        
        public WordViewModel SelectedWord
        {
            get { return _SelectedWord; }
            set
            {
                if (_SelectedWord != value)
                {
                    _SelectedWord = value;
                    OnPropertyChanged("SelectedWord");
                }
            }
        }
        public string SearchedText
        {
            get { return _SearchedText; }
            set
            {
                if (_SearchedText != value)
                {
                    _SearchedText = value;
                    OnPropertyChanged("SearchedText");
                }
            }
        }
        #endregion

        #region constructor
        public WordsListViewModel()
        {
            AllWords = new ObservableCollection<WordViewModel>();
            WordsToDisplay = new ObservableCollection<WordViewModel>();
            if (AllWords.Count == 0)
            {
                GetData();
            }
            AddWordCommand = new Command(AddWord);
            BackCommand = new Command(Back);
            SaveWordCommand = new Command(SaveWord);
            SearchWordCommand = new Command(SearchCommandExecute);
            LoadWords();
        }
        #endregion

        #region WordsListViewModel methods
        private void SearchCommandExecute()
        {
            if (string.IsNullOrWhiteSpace(this._SearchedText)||string.IsNullOrEmpty(this._SearchedText))
            {
                WordsToDisplay = new ObservableCollection<WordViewModel>();
                LoadWords();
            }
            else
            {
                var SearchResults = new ObservableCollection<WordViewModel>(AllWords.Where(i => i.WordInEnglish.ToLower().Contains(SearchedText.ToLower())));
                foreach(var x in SearchResults)
                {
                    Console.WriteLine(x.WordInEnglish);
                }
                WordsToDisplay = SearchResults;
            }
        }

        public void SaveWord(object wordObject)
        {
            WordViewModel word = wordObject as WordViewModel;
            if (word != null && word.ModelIsValid())
            {
                word.Transcription = "[" + word.Transcription + "]";
                AllWords.Insert(0,word);
                WordsToDisplay = new ObservableCollection<WordViewModel>();
                LoadWords();
                SearchedText = null;
                Back();
            }
            else
            {
                MessagingCenter.Send(this, "InputValidation");
            }
        }

        public void AddWord()
        {
            Navigation.PushAsync(new WordPage(this));
        }

        public void Back()
        {
            Navigation.PopAsync();
        }
               
        public void ViewTags()
        {
            Navigation.PushAsync(new TagsListPage(this));
        }

        public void LoadWords()
        {
            int num = WordsToDisplay.Count;
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                for (int i = num; i < num + 15; i++)
                {
                    if (i >= AllWords.Count)
                    {
                        break;
                    }
                    WordsToDisplay.Add(AllWords[i]);
                }
                return false;
            });                          
        }
        private void GetData()
        {
            AllWords.Add(new WordViewModel("abandon", "əˈbændən", "залишати щось","verbs","topic1"));
            AllWords.Add(new WordViewModel("back", "'bæk", "спина", "body", "topic1"));
            AllWords.Add(new WordViewModel("neck ", "'nek", "шия", "body"));
            AllWords.Add(new WordViewModel("orange", "'ɒrɪndʒ", "оранжевий", "colors", "topic1"));
            AllWords.Add(new WordViewModel("blue", "'blu:", "блакитний", "colors"));
            AllWords.Add(new WordViewModel("green", "'gri:n", "зелений", "colors"));
            AllWords.Add(new WordViewModel("grey", "'greɪ", "сірий", "colors"));
            AllWords.Add(new WordViewModel("black", "'blæk", "чорний", "colors"));
            AllWords.Add(new WordViewModel("white", "'waɪt", "білий", "colors"));
            AllWords.Add(new WordViewModel("nose", "nouz", "ніс", "body"));
            AllWords.Add(new WordViewModel("hate", "heɪt", "ненавидеть", "verbs"));
            AllWords.Add(new WordViewModel("load", "loʊd", "завантажувати", "verbs", "topic1"));
            AllWords.Add(new WordViewModel("prepare", "prɪˈper", "готуватися", "verbs"));
            AllWords.Add(new WordViewModel("gold", "ɡoʊld", "золотий", "colors"));
            AllWords.Add(new WordViewModel("leap ", "liːp", "стрибати", "verbs"));
            AllWords.Add(new WordViewModel("one", "wʌn", "один", "numbers"));
            AllWords.Add(new WordViewModel("two", "tuː", "два", "numbers"));
            AllWords.Add(new WordViewModel("three", "θriː", "три", "numbers", "topic1"));
            AllWords.Add(new WordViewModel("four", "fɔːr", "чотири", "numbers"));
            AllWords.Add(new WordViewModel("five", "five", "п'ять", "numbers"));
            AllWords.Add(new WordViewModel("six", "sɪks", "шість", "numbers"));
            AllWords.Add(new WordViewModel("seven", "ˈsevn", "сім", "numbers", "topic1"));
            AllWords.Add(new WordViewModel("eight", "eɪt", "вісім", "numbers"));
            AllWords.Add(new WordViewModel("nine", "naɪn", "дев'ять", "numbers"));
            AllWords.Add(new WordViewModel("zero", "ˈzɪroʊ", "нуль", "numbers"));
            AllWords.Add(new WordViewModel("arm", "ɑːrm", "рука", "body"));
            AllWords.Add(new WordViewModel("leg", "leɡ", "нога", "body"));
            AllWords.Add(new WordViewModel("head", "hed", "голова", "body", "topic1"));
            AllWords.Add(new WordViewModel("feet", "fiːt", "стопи", "body"));
            AllWords.Add(new WordViewModel("finger", "ˈfɪŋɡər", "палець", "body"));
            AllWords.Add(new WordViewModel("chest", "tʃest", "грудна клітка", "body"));
            AllWords.Add(new WordViewModel("pen", "pen", "ручка", "school"));
            AllWords.Add(new WordViewModel("pencil", "ˈpensl", "олівець", "school", "topic1"));
            AllWords.Add(new WordViewModel("pencil-case", "ˈpenslkeɪs", "пенал", "school"));
            AllWords.Add(new WordViewModel("desk", "desk", "парта", "school"));
            AllWords.Add(new WordViewModel("backpack", "ˈbækpæk", "ранець", "school"));
            AllWords.Add(new WordViewModel("vacation", "vəˈkeɪʃn", "канікули", "school"));
            AllWords.Add(new WordViewModel("exam", "ɪɡˈzæm", "екзамен", "school"));
            AllWords.Add(new WordViewModel("happy", "ˈhæpi", "щасливий", "feelings", "topic1"));
            AllWords.Add(new WordViewModel("sad", "sæd", "сумний", "feelings"));
            AllWords.Add(new WordViewModel("angry", "ˈæŋɡri", "сердитий", "feelings"));
            AllWords.Add(new WordViewModel("scared", "skerd", "наляканий", "feelings"));
            AllWords.Add(new WordViewModel("surprised", "sərˈpraɪzd", "здивований", "feelings"));
            AllWords.Add(new WordViewModel("walk", "wɔːk", "прогулюватися", "verbs"));
            AllWords.Add(new WordViewModel("follow", "ˈfɑːloʊ", "слідувати", "verbs", "topic1"));
        }
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
