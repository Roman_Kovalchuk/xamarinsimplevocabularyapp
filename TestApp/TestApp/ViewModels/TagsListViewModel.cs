﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using System.Text;
using Xamarin.Forms;
using System.Linq;

namespace TestApp.ViewModels
{
    public class TagsListViewModel:INotifyPropertyChanged
    {
        #region TagsListViewModel attributes
        private string _SearchedText;
        private ObservableCollection<string> _AllTags;
        public event PropertyChangedEventHandler PropertyChanged;
        private WordsListViewModel _WordsListViewModel;
        private string _SelectedTag;
        private ObservableCollection<string> _TagsToDisplay;
        #endregion

        #region TagsListViewModel properties
        public ObservableCollection<string> TagsToDisplay
        {
            get
            {
                return _TagsToDisplay;
            }
            set
            {
                if (_TagsToDisplay != value)
                {
                    _TagsToDisplay = value;
                    OnPropertyChanged("TagsToDisplay");
                }
            }
        }
        public string SelectedTag
        {
            get
            {
                return _SelectedTag;
            }
            set
            {
                if (_SelectedTag != value)
                {
                    _SelectedTag = value;
                    WordsListViewModel.SearchedText = value;
                    OnPropertyChanged("SelectedTag");
                    WordsListViewModel.Navigation.PopAsync();
                }
            }
        }
        public WordsListViewModel WordsListViewModel
        {
            get
            {
                return _WordsListViewModel;
            }
            set
            {
                if (_WordsListViewModel != value)
                {
                    _WordsListViewModel = value;
                    OnPropertyChanged("WordsListViewModel");
                }
            }
        }
        public ObservableCollection<string> AllTags
        {
            get
            {
                return _AllTags;
            }
            set
            {
                if (_AllTags != value)
                {
                    _AllTags = value;
                    OnPropertyChanged("AllTags");
                }
            }
        }
        public string SearchedText
        {
            get { return _SearchedText; }
            set
            {
                if (_SearchedText != value)
                {
                    _SearchedText = value;
                    OnPropertyChanged("SearhcedText");
                }
            }
        }
        #endregion

        #region TagsListViewModel constructor
        public TagsListViewModel(WordsListViewModel wordsListViewModel)
        {
            AllTags = new ObservableCollection<string>();
            this.WordsListViewModel = wordsListViewModel;
            foreach(WordViewModel word in this.WordsListViewModel.AllWords)
            {
                foreach(string tag in word.Tags)
                {
                    AllTags.Add(tag);
                }
            }
            AllTags = new ObservableCollection<string>(AllTags.Distinct() );
            TagsToDisplay = AllTags;
        }
        #endregion

        #region TagsListViewModel method
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
