﻿using System;
using System.Collections.ObjectModel;
using System.Text;
using System.ComponentModel;
using System.Windows.Input;
using TestApp.Models;
using Xamarin.Forms;
using TestApp.Pages;

namespace TestApp.ViewModels
{
    public class WordViewModel:INotifyPropertyChanged
    {
        #region WordViewModel attributes
        public event PropertyChangedEventHandler PropertyChanged;
        private Word Word;
        private string _TempAdditionalTag;
        private WordsListViewModel _WordsListViewModel;
        #endregion

        #region WordViewModel properties

        public ICommand AddAdditionalTagCommand { get; set; }

        public WordsListViewModel WordsListViewModel
        {
            get { return _WordsListViewModel; }
            set
            {
                if (_WordsListViewModel != value)
                {
                    _WordsListViewModel = value;
                    OnPropertyChanged("WordsListViewModel");
                }
            }
        }

        
        public string TempAdditionalTag
        {
            get
            {
                return _TempAdditionalTag;
            }
            set
            {
                if (_TempAdditionalTag != value)
                {
                    _TempAdditionalTag = value;
                    OnPropertyChanged("TempAdditionalTag");
                }
            }
        }
        public string WordInEnglish
        {
            get { return Word.WordInEnglish; }
            set
            {
                if (Word.WordInEnglish != value)
                {
                    Word.WordInEnglish = value;
                    OnPropertyChanged("WordInEnglish");
                }
            }
        }

        public string Transcription
        {
            get { return Word.Transcription; }
            set
            {
                if (Word.Transcription != value)
                {
                    Word.Transcription = value;
                    OnPropertyChanged("Transcription");
                }
            }
        }

        public string MainTranslation
        {
            get { return Word.MainTranslation; }
            set
            {
                if (Word.MainTranslation != value)
                {
                    Word.MainTranslation = value;
                    OnPropertyChanged("MainTranslation");
                }
            }
        }
        public ObservableCollection<string> Tags
        {
            get { return Word.Tags; }
            set
            {
                if (Word.Tags != value)
                {
                    Word.Tags = value;
                    OnPropertyChanged("Tags");
                }
            }
        }
        #endregion

        #region WordViewModel constructors
        public WordViewModel()
        {
            Word = new Word();
            AddAdditionalTagCommand = new Command(AddTag);
        }

        public WordViewModel(string wordInEnglish, string transcription, string translation,params string[] anotherTags)
        {
            Word = new Word();
            for(int i=0;i< anotherTags.Length; i++)
            {
                this.Tags.Add("#"+anotherTags[i]);
            }
            this.Word.MainTranslation = translation;
            this.Word.Transcription = "["+ transcription + "]";
            this.Word.WordInEnglish = wordInEnglish;
        }
        #endregion

        #region WordViewModel methods
        public void AddTag()
        {
            if (!string.IsNullOrEmpty(TempAdditionalTag))
            {
                Tags.Add("#"+TempAdditionalTag);
                OnPropertyChanged("AdditionalTags");
            }
        }
        public bool ModelIsValid()
        {
            bool isValid = false;

            try
            {
                if (!string.IsNullOrEmpty(this.WordInEnglish.Trim()) && !string.IsNullOrEmpty(this.MainTranslation.Trim())
                && !string.IsNullOrEmpty(this.Transcription.Trim()) && !(Tags.Count==0))
                    isValid = true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            return isValid;
        }
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
