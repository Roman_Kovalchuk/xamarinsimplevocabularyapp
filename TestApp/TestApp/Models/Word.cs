﻿using System;
using System.Collections.ObjectModel;
using System.Text;

namespace TestApp.Models
{
    public class Word
    {
        public string WordInEnglish { get; set; }
        public string Transcription { get; set; }
        public string MainTranslation { get; set; }
        public ObservableCollection<string> Tags { get; set; }
        public Word()
        {
            Tags = new ObservableCollection<string>();
        }
    }
}
